# KSM V2 assembler

This assembler is written in python, and outputs nasm assembly code. This code must be assembled into an object file and linked with the object file of `src/helpers.c` and `libc`. 

## helper.c

`helper.c` is written in POSIX C99, and should run on any system with POSIX libaries.

## Running the compiler

You will need:

- Python 3.7+
- GCC 10+
- NASM assembler

Run the program like so:

```
$ chmod 755 ksmc
$ ./ksmc <path to ksm file> [output name, 0 for default] [keep tmp dir: 1 or 0]
$ ./<output name>
default:
$ ./output
```


## Compatability

Current this compiler works on:

- x86_64 GLIBC Void Linux

It *should* run on:

- x86_64 FreeBSD w. Linux compatability layer
- Any x86_64 linux distro
