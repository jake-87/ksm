from dataclasses import dataclass
import re
import cursed
import sys
import ins_generators as ins_gen
# helper
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
@dataclass
class tok_ret():
    data: str
    text: str
# Helper function that just returns some stuff we need, eg memory allocation
def prelude(k: int) -> str:
    return [f"""
bits 64
section .bss
    argv resq 1
    argc resq 1
    pointer resq 1
section .data
    \n""", f"""
section .text
    global main
    extern malloc
    extern free
    extern print_int
    extern print_hex
    extern print_string
    extern print_newline
    extern print_char
    extern open_file
    extern file_getchar
    extern file_feof
main:
    mov qword [argc], rdi
    mov rax, rsi
    mov qword [argv], rax
    mov rax, {k}
    mov rbx, 8
    imul rax, rbx
    mov rdi, rax
    call malloc
    mov qword [pointer], rax
    mov qword rcx, [pointer]
; end prelude
"""]

# Generate a token
def gen(status: ins_gen.status_t, old_tok: str, next: str = "") -> str:
    tok = re.split("\\s+(?![^\\[]*\\])", old_tok.replace(",", ""))
    # Data section, text section
    ret = tok_ret("", "")

    # We're in a raw segement
    if status.is_in_raw:
        if tok[0] == "{":
            # the { doesn't actually do anything, just looks nice
            return tok_ret("", "")
        if tok[0] == "}":
            # End of raw asm segment
            status.is_in_raw = False
            status.rcx = 1
            return tok_ret("", "\n\n; End of raw_asm segment\n\n")
        # Load / store a value from ksm's memory
        if tok[0] in ("ksm_load", "ksm_store"):
            return tok_ret("", "\n" + ins_gen.gen_ksm_raw_mov(tok[1], tok[2]))
        # Otherwise, just print it
        return tok_ret("", old_tok + "\n")
    else:
        # Only check for raw_asm if we're not in one
        if tok[0] == "raw_asm":
            status.is_in_raw = True
            return tok_ret("", "\n\n; Start of raw_asm segment\n")
    # rcx has been modified
    if status.rcx:
        ret.text += f"   mov qword rcx, [pointer]\n"
        status.rcx = 0
    # regular instructions
    if tok[0] == "mov":
        ret.text += ins_gen.gen_mov(tok[1], tok[2])
    elif tok[0] == "add":
        ret.text += ins_gen.gen_math(tok[1], tok[2], "add")
    elif tok[0] == "sub":
        ret.text += ins_gen.gen_sub(tok[1], tok[2], "sub")
    elif tok[0] == "mul":
        ret.text += ins_gen.gen_math(tok[1], tok[2], "imul")
    elif tok[0] == "shl":
        ret.text += ins_gen.gen_shift(status, tok[1], tok[2], "sal")
    elif tok[0] == "shr":
        ret.text += ins_gen.gen_shift(status, tok[1], tok[2], "sar")
    elif tok[0] == "div":
        ret.text += ins_gen.gen_divmod(tok[1], tok[2], 1)
    elif tok[0] == "mod":
        ret.text += ins_gen.gen_divmod(tok[1], tok[2], 0)
    elif tok[0] == "cmp":
        ret.text += ins_gen.gen_cmp(tok[1], tok[2])
    elif tok[0] == "hlt":
        ret.text += ins_gen.gen_hlt(tok[1])
    elif tok[0] == "open_file":
        ret.text += ins_gen.gen_open_file(status, tok[1], tok[2])
    elif tok[0] == "file_getchar":
        ret.text += ins_gen.gen_file_getchar(status, tok[1], tok[2])
    elif tok[0] == "file_feof":
        ret.text += ins_gen.gen_file_feof(status, tok[1], tok[2])
    elif tok[0] == "func":
        ret.text += ins_gen.gen_func(status, tok[1])
    elif tok[0] == "return":
        ret.text += ins_gen.gen_return(tok[1])
    elif tok[0] == "call":
        ret.text += ins_gen.gen_call_func(status, tok)
    elif tok[0] == "print_string":
        # TLDR: This, in order: Joins the remaining tokens that were split earlier, 
        # removes the first one, which is the command, then removes the first and last char
        # which should always be the quotes around the string.
        tok = [tok[0], " ".join(tok[1:len(tok)])[1:-1]]
        data, text = ins_gen.gen_print_string(status, tok[1])
        ret.text = ret.text + text
        ret.data = data
    elif tok[0] == "print_newline":
        ret.text += ins_gen.gen_print_newline(status)
    # Stack instructions
    elif tok[0] in ("pop", "peek", "push"):
        ret.text += ins_gen.gen_stack(tok[1], tok[0])
    # Control flow instructions
    elif tok[0] in ("while", "if"):
        ret.text += ins_gen.gen_control_flow(status, tok[0], tok[1], tok[2], tok[3])
    elif tok[0] in ("}", "else"):
        ret.text += ins_gen.gen_else(status, tok[0], next)
    # Printers
    # print_char just takes an int operand and converts it to char, so it's all good
    elif tok[0] in ("print_int", "print_hex", "print_char"):
        ret.text += ins_gen.gen_print_int(status, tok[1], tok[0])
    # Jumps
    elif tok[0] in ("jmp", "je", "jne", "jg", "jl"):
        # jumps are already correct asm syntax ( i wonder why... )
        ret.text += "   " + " ".join(tok) + "\n"
    else:
        # Could be a lable, could be some random stuff, so we just print it
        ret.text += " ".join(tok) + "\n"
    
    # Add comments to nasm so we can see what does what
    ret.text = f"\n" + ret.text + f"\n; {old_tok}\n" 
    return (ret)