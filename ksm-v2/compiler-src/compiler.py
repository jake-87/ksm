import sys
import generate_asm
import ins_generators as ins_gen
import re
# helper
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
def main():
    with open(sys.argv[1]) as fp:
        data = fp.read()
    data = data.split('\n')
    status = ins_gen.status_t()
    nasm_code = ""
    data_section = ""
    # Prelude returns [data_section, nasm_code]
    pre = generate_asm.prelude(int(sys.argv[2]))
    data_section += pre[0]
    nasm_code += pre[1]
    i = 0
    next_helper = [j for j in range(len(data))]
    for asm in data:
        next_helper[i] = asm.strip()
        i += 1
    i = 0
    for asm in data:
        try:
            # Get next token
            next = next_helper[i + 1].split(" ")[0]
        except IndexError:
            next = ""
        asm = asm.strip()
        tmp = generate_asm.gen(status, asm, next)
        nasm_code += tmp.text
        data_section += tmp.data
        i += 1
    final = data_section + "\n ; end data section\n" + nasm_code
    print(final)
    exit(0)
if __name__ == "__main__":
    main()