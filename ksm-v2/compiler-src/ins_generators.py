from dataclasses import dataclass
import sys
import hashlib # for strings
# helper
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)
class status_t():
    is_in_raw: bool = 0
    rcx: bool = 0
    register_list = ("r9", "r10", "r11", "r12", "r13", "r14", "r15")
    # Stands for Unrestriced Extension
    ue = ("r9", "r10", "r11", "r12", "r13", "r14", "r15", "rax","rbx","rcx","rdx","rsi","rdi","rsp","rbp")
    strings = {}
    control_flow_alias = {
        "==": ("je", "jne"),
        "!=": ("jne", "je"),
        ">": ("jg", "jle"),
        "<": ("jl", "jge"),
    }
    control_flow_stack = []
    control_flow_inc = 0
    lable_has_else = {}
    last_else = ""

# Self explanitory. Also accounts for hex.
def is_valid_int(m: str) -> bool:
    if "x" in m:
        return True
    elif m.isdigit():
        return True
    else:
        return False

# Converts a ksm memory loc or number into decimal
def td(n: str) -> int:
    if "m" in n and "x" in n:
        return int(n[1:len(n)], 16)
    elif "m" in n:
        return int(n[1:len(n)])
    elif "x" in n:
        return int(n, 16)
    else:
        return int(n)

# Moves first argument into given register. `ue` lets it set to any register, not just KSM allowed registers.
def mov_arg_to_reg(a: str, reg: str, ue: bool = False) -> str:
    if a in status_t.register_list or ue and a in status_t.ue:
        if "[" in a:
            return f"   mov qword {reg}, [rcx + {a[1:-1]} * 8]\n"
        else:
            return f"   mov qword {reg}, {a}\n"
    elif "m" in a:
        if "[" in a:
            fin = f"   mov qword rbx, [rcx + {td(a[1:-1])} * 8]\n"
            return fin + f"   mov qword {reg}, [rcx + rbx * 8]\n"
        else:
            return f"   mov qword {reg}, [rcx + {td(a)} * 8]\n"
    else:
        if "[" in a:
            return f"   mov qword {reg}, [rcx + {td(a[1:-1])} * 8]\n"
        else:
            return f"   mov qword {reg}, {td(a)}\n"

# Moves from reg into first argument. See `ue` above.
def mov_reg_from_arg(a: str, reg: str, ue: bool = False) -> str:
    if a in status_t.register_list or ue and a in status_t.ue:
        return f"   mov qword {a}, {reg}\n"
    elif "m" in a:
        return f"   mov qword [rcx + {td(a)} * 8], {reg}\n"
    else:
        return f"   mov qword {td(a)}, {reg}\n"

# Generates move instructions
def gen_mov(a1: str, a2: str, ue: bool = False) -> str:
    fin = ""
    # a2 is src. a1 is dest.
    if a1 in status_t.ue and a2 in status_t.ue:
        # no need to do two movs for reg -> reg
        fin += f"   mov {a1}, {a2}\n"
    elif a1 in status_t.ue and is_valid_int(a2):
        # same, but for int -> reg
        fin += f"   mov {a1}, {td(a2)}" 
    else:
        fin += mov_arg_to_reg(a2, "rax", ue)
        fin += mov_reg_from_arg(a1, "rax", ue)
    return fin

# Raw mov for use in `raw_asm` section. Accounts for allowance of use of `rax`, etc.
def gen_ksm_raw_mov(a1: str, a2: str) -> str:
    fin = ""
    if a1 == "rax":
        tmp = "rbx"
    else:
        tmp = "rax"
    fin += f"   push {tmp}\n"
    fin += mov_arg_to_reg(a2, tmp, True)
    fin += mov_reg_from_arg(a1, tmp, True)
    fin += f"   pop {tmp}"
    return fin

# Generate math instruction
def gen_math(a1: str, a2: str, op: str) -> str:
    fin = ""
    # a2 is src. a1 is dest.
    fin += mov_arg_to_reg(a2, "rax")
    fin += mov_arg_to_reg(a1, "rbx")
    fin += f"   {op} rax, rbx\n"
    fin += mov_reg_from_arg(a1, "rax")
    return fin

def gen_sub(a1: str, a2: str, op: str) -> str:
    fin = ""
    # a2 is src. a1 is dest.
    fin += mov_arg_to_reg(a2, "rax")
    fin += mov_arg_to_reg(a1, "rbx")
    fin += f"   {op} rbx, rax\n"
    fin += mov_reg_from_arg(a1, "rbx")
    return fin


def gen_shift(st: status_t, a1: str, a2: str, op: str) -> str:
    fin = ""
    # a2 is src. a1 is dest.
    fin += mov_arg_to_reg(a2, "rcx")
    fin += mov_arg_to_reg(a1, "rax")
    fin += f"   {op} rax, cl\n"
    fin += mov_reg_from_arg(a1, "rax")
    st.rcx = 1
    return fin

# Generate compare instruction
def gen_cmp(a1: str, a2: str) -> str:
    fin = ""
    # a2 is src. a1 is dest.
    fin += mov_arg_to_reg(a2, "rax")
    fin += mov_arg_to_reg(a1, "rbx")
    fin += f"   cmp rax, rbx\n"
    return fin

# Generate division or modulo instruction
def gen_divmod(a1: str, a2: str, m: int) -> str:
    fin = ""
    # a2 is src. a1 is dest. m decides whether to divide or modulo
    fin += mov_arg_to_reg(a2, "rax")
    fin += mov_arg_to_reg(a1, "rbx")
    fin += f"   cqo\n"
    fin += f"   idiv rbx\n"
    if m == 1:
        reg = "rdx"
    else:
        reg = "rax"
    fin += mov_reg_from_arg(a1, reg)
    return fin

# Generate stack instruction
def gen_stack(a1: str, m: str) -> str:
    fin = ""
    # Peeks are just pop followed by push
    if m == "pop" or m == "peek":
        if a1 in status_t.ue:
            # we can just pop registers
            fin += f"   pop {a1}"
        else:
            fin += f"   pop rax\n"
            fin += gen_mov(a1, "rax", True)
    elif m == "push" or m == "peek":
        if a1 in status_t.ue:
            # we can just push registers
            fin += f"   push {a1}\n"
        elif is_valid_int(a1):
            # we can also just push integers
            fin += f"   push {td(a1)}"
        else:
            fin += mov_arg_to_reg(a1, "rax")
            fin += f"   push rax\n"
    return fin

# Generate halt
def gen_hlt(a1: str) -> str:
    fin = ""
    # free the memory we allocated earlier
    fin += f"   mov rdi, [pointer]\n"
    fin += f"   call free\n"
    fin += mov_arg_to_reg(a1, "rdi")
    fin += f"   mov qword rax, 60\n"
    fin += f"   syscall\n"
    return fin

# These two functions are needed because three of the user registers we have defined, r9-11
# are technically scratch registers and do not need to stay their current values,
# therefor we have to store and restore them apon syscalls and the like
def pre_syscall():
    fin = ""
    fin += f"   push r9\n"
    fin += f"   push r10\n"
    fin += f"   push r11\n"
    return fin

def post_syscall():
    fin = ""
    fin += f"   pop r11\n"
    fin += f"   pop r10\n"
    fin += f"   pop r9\n"
    fin += f"   mov qword rcx, [pointer]\n"
    return fin

# Generate interger print
def gen_print_int(st: status_t, a1: str, print: str) -> str:
    fin = ""
    fin += pre_syscall()
    fin += mov_arg_to_reg(a1, "rdi")
    fin += f"   call {print}\n"
    fin += post_syscall()
    st.rcx = 1
    return fin


# Print a string
def gen_print_string(st: status_t, a1: str) -> list:
    fin = ["", ""]
    fixed = a1.replace('\\n', '", 0xa, "')
    fin[1] += pre_syscall()
    # The sha512 just makes it much less likly to collide, although does up compile times
    better_a1 = hashlib.sha512(a1.encode()).hexdigest() + "__ksm_internal_string"
    # If we don't already have the string, make it
    if better_a1 not in st.strings:
        st.strings[better_a1] = better_a1
        # add null byte at end, C style
        fin[0] += f'    _{better_a1}_: db "{fixed}", 0\n'
    fin[1] += f"   mov qword rdi, _{better_a1}_\n"
    fin[1] += f"   call print_string\n"
    fin[1] += post_syscall()
    st.rcx = 1
    return fin


# Print a newline
def gen_print_newline(st: status_t) -> str:
    fin = ""
    fin += pre_syscall()
    fin += f"   call print_newline\n"
    fin += post_syscall()
    st.rcx = 1
    return fin

# Opens a file
def gen_open_file(st: status_t, a1: str, a2: str) -> str:
    fin = ""
    fin += pre_syscall()
    fin += f"   mov qword rdi, [argv]\n"
    fin += mov_arg_to_reg(a2, "rsi")
    fin += f"   call open_file\n"
    fin += post_syscall()
    fin += mov_reg_from_arg(a1, "rax")
    st.rcx = 1
    return fin

# Gets a char from a FILE *
def gen_file_getchar(st: status_t, a1: str, a2: str) -> str:
    fin = ""
    fin += pre_syscall()
    fin += mov_arg_to_reg(a2, "rdi")
    fin += f"   call file_getchar\n"
    fin += post_syscall()
    fin += mov_reg_from_arg(a1, "rax")
    st.rcx = 1
    return fin

# `feof`s a FILE *
def gen_file_feof(st: status_t, a1: str, a2: str) -> str:
    fin = ""
    fin += pre_syscall()
    fin += mov_arg_to_reg(a2, "rdi")
    fin += f"   call file_feof\n"
    fin += post_syscall()
    fin += mov_reg_from_arg(a1, "rax")
    st.rcx = 1
    return fin
    
# Generate while and if statements
def gen_control_flow(st: status_t, a1: str, a2: str, a3: str, a4: str) -> str:
    fin = ""
    jmp_condition = st.control_flow_alias[a3]
    lable = f"{a1}__{a2}_{a4}_{st.control_flow_inc}"
    compare = mov_arg_to_reg(a2, "rax") + mov_arg_to_reg(a4, "rbx") + f"   cmp rax, rbx\n"
    st.control_flow_inc += 1
    if a1 == "while":
        type = "w"
        fin += f"{lable}:\n"
        fin += compare
        fin += f"   {jmp_condition[1]} {lable}_done\n"
    elif a1 == "if":
        type = "i"
        fin += compare
        # Jump to the else if it's false
        # The else also functions as the end of the if if it doesn't have an else
        # The mov serves as the method for storing whether to jump to done or not
        fin += f"   mov rax, 0\n"
        fin += f"   {jmp_condition[1]} {lable}_else\n"
        # Default to no; a later else can change this
        st.lable_has_else[lable] = 0
    stack_tuple: tuple = (type, lable)
    st.control_flow_stack += [(stack_tuple)]
    return fin

# Generate Else statement
# This also generates brackets; using internal state variables in order to properly generate if-else structures
def gen_else(st: status_t, tok: str, next: str) -> str:
    fin = ""
    if tok != "else":
        top = st.control_flow_stack.pop()
    else:
        top = [0, 0]
        fin += f"; this was an else\n"
    if top[0] == "w":
        fin += f"   jmp {top[1]}\n"
        fin += f"{top[1]}_done:\n"
    elif top[0] == "i":
        if tok == "}" and next == "else":
            # We're in an else branch
            fin += f"   mov rax, 1\n"
            fin += f"{top[1]}_else:\n"
            fin += f"   cmp rax, 1\n"
            fin += f"   je {top[1]}_done\n"
            st.lable_has_else[top[1]] = 1
            st.control_flow_stack += [("i", top[1])]
        else:
            # We're at the end of an if-else statement
            if st.lable_has_else[top[1]]:
                fin += f"{top[1]}_done:\n"
            else:
                fin += f"{top[1]}_else:\n"
        fin += f"   ; endif\n"
    elif top[0] == "f":
        fin += f"{top[1]}:\n"
    return fin


def gen_func(st: status_t, name: str) -> str:
    fin = ""
    fin += f"   jmp {name}_done\n"
    fin += name + f":\n"
    st.control_flow_stack += [("f", f"{name}_done")]
    return fin

def gen_return(ret: str) -> str:
    fin = ""
    fin += mov_arg_to_reg(ret, "rax")
    fin += f"   ret\n"
    return fin

def gen_call_func(st: status_t, full_tok: list) -> str:
    fin = ""
    # [0]  = call
    # [1]  = return place
    # [2]  = function name
    # [3+] = args
    # Minus three for the `call` itself, the return, and the function name
    l = len(full_tok) - 3
    i = 0
    for reg in st.register_list:
        if l == 0:
            break
        fin += mov_arg_to_reg(full_tok[i + 3], reg)
        i += 1
        if i >= l:
            break
    # two is the function name
    fin += f"   call {full_tok[2]}\n"
    fin += mov_reg_from_arg(full_tok[1], "rax")
    return fin