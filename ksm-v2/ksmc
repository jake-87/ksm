#! /usr/bin/env python3
import sys
from io import StringIO 
import subprocess
import os
import time
def run_command(args: list):
    ret = subprocess.run(args, capture_output=True)
    stdout = ret.stdout.decode("utf-8")
    stderr = ret.stderr.decode("utf-8")
    if stderr.strip():
        print("Command", args, f"\n threw errors:\n", stderr)
        exit(1)
    return stdout
def main():
    if len(sys.argv) < 2:
        print("USAGE: ./ksmc <path to ksm file> [output name, 0 for default] [keep files]")
        exit(1)
    else:
        all_start = time.perf_counter()
        file = sys.argv[1]
        with open(file, "r") as f:
            data = f.read()
        split = data.replace(",", " ").replace("[", "").replace("]", "").replace("\n", " ").split(" ")
        test = [0 for i in range(len(split))]
        count = 0
        # This finds the largest memory location in the code, so that we can make the memory only that big
        for k in split:
            b = k.strip()
            # only look at mem locations
            if b and b[0] == "m":
                try:
                    # Set test count to the value of the mem location
                    test[count] = int(b[1:len(b)])
                    count += 1
                # it's not a mem location, but an instrucion or smth
                except ValueError:
                    pass
        test = [i for i in test if i]
        test.sort()
        # Make mem the largest memory location in the file
        if test:
            mem = test[-1]
        else:
            mem = 1
        # Make temp path
        if not os.path.isdir("./tmp"):
            os.mkdir("./tmp")


        start = time.perf_counter()
        # Run the actual compiler
        stdout = run_command(["python3", "compiler-src/compiler.py", sys.argv[1], str(mem)])
        with open("./tmp/output.nasm", "w") as f:
            f.write(stdout)
        end = time.perf_counter()
        print("KSM compiling took   ", round((end - start) * 1000, 5), "milliseconds")


        # Compile the helper
        start = time.perf_counter()
        stdout = run_command(["cc", "compiler-src/helpers.c", "-o", "tmp/helpers.o", "-c", "-Wall", "-Wextra", "-fPIC", "-g"])
        if stdout.strip():
            print(stdout)
        end = time.perf_counter()
        print("Helper compiling took", round((end - start) * 1000, 5), "milliseconds")


        start = time.perf_counter()
        stdout = run_command(["nasm", "-f", "elf64", "tmp/output.nasm", "-o", "tmp/output.o", "-F", "dwarf"])
        if stdout.strip():
            print(stdout)
        if len(sys.argv) >= 3 and sys.argv[2] != "0":
            output = sys.argv[2]
        else:
            output = "output"
        end = time.perf_counter()


        print("Assembling took      ", round((end - start) * 1000, 5), "milliseconds")
        # stdout = run_command([ "ld", "-o", output, "-dynamic-linker", "/lib/ld-linux-x86-64.so.2",
        #                      "/usr/lib/crt1.o", "/usr/lib/crti.o", "-lc", "tmp/output.o",
        #                       "tmp/helpers.o", "/usr/lib/crtn.o"])
        start = time.perf_counter()
        stdout = run_command(["cc", "-o", "output", "tmp/output.o", "tmp/helpers.o", "-static", "-g"])
        if stdout.strip():
            print(stdout)
        if not len(sys.argv) == 4:
            stdout = run_command(["rm", "-rf", "./tmp"])
        end = time.perf_counter()
        print("Linking took         ", round((end - start) * 1000, 5), "milliseconds")


        all_end = time.perf_counter()
        print("Full took            ", round((all_end - all_start) * 1000, 5), "milliseconds")
if __name__ == "__main__":
    main()