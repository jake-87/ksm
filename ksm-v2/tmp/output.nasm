
bits 64
section .bss
    argv resq 1
    argc resq 1
    pointer resq 1
section .data
    

 ; end data section

section .text
    global main
    extern malloc
    extern free
    extern print_int
    extern print_hex
    extern print_string
    extern print_newline
    extern print_char
    extern open_file
    extern file_getchar
    extern file_feof
main:
    mov qword [argc], rdi
    mov rax, rsi
    mov qword [argv], rax
    mov rax, 1
    mov rbx, 8
    imul rax, rbx
    mov rdi, rax
    call malloc
    mov qword [pointer], rax
    mov qword rcx, [pointer]
; end prelude

   push r9
   push r10
   push r11
   mov qword rdi, [argv]
   mov qword rsi, 1
   call open_file
   pop r11
   pop r10
   pop r9
   mov qword rcx, [pointer]
   mov qword [rcx + 0 * 8], rax

; open_file m0, 1

   mov qword rcx, [pointer]
   mov r9, 0
; mov r9, 0

while__r9_0_0:
   mov qword rax, r9
   mov qword rbx, 0
   cmp rax, rbx
   jne while__r9_0_0_done

; while r9 == 0 {

   push r9
   push r10
   push r11
   mov qword rdi, [rcx + 0 * 8]
   call file_getchar
   pop r11
   pop r10
   pop r9
   mov qword rcx, [pointer]
   mov qword [rcx + 1 * 8], rax

; file_getchar m1, m0

   mov qword rcx, [pointer]
   push r9
   push r10
   push r11
   mov qword rdi, [rcx + 0 * 8]
   call file_feof
   pop r11
   pop r10
   pop r9
   mov qword rcx, [pointer]
   mov qword r9, rax

; file_feof r9, m0

   mov qword rcx, [pointer]
   mov qword rax, r9
   mov qword rbx, 0
   cmp rax, rbx
   mov rax, 0
   jne if__r9_0_1_else

; if r9 == 0 {

   push r9
   push r10
   push r11
   mov qword rdi, [rcx + 1 * 8]
   call print_char
   pop r11
   pop r10
   pop r9
   mov qword rcx, [pointer]

; print_char m1

   mov qword rcx, [pointer]
if__r9_0_1_else:
   ; endif

; }

   jmp while__r9_0_0
while__r9_0_0_done:

; }

   push r9
   push r10
   push r11
   call print_newline
   pop r11
   pop r10
   pop r9
   mov qword rcx, [pointer]

; print_newline

   mov qword rcx, [pointer]
   mov rdi, [pointer]
   call free
   mov qword rdi, 0
   mov qword rax, 60
   syscall

; hlt 0

