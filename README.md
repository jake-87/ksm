# ksm

Specs and example of the KSM programming language.

## ksm-v1 

KSM-v1 was a VM based language with a simple assembler. It is now finished. Please report any bugs at https://gitlab.com/jake-87/ksm/-/issues

Do not use ksm-v1. It has several fundemental flaws. Use v2.

## ksm-v2

KSM-v2 is a compiled language based on the design of KSM-v1. It is finished, and is the current version of ksm. Please refer to ksm-v2/docs/* for documentation and help. Please report bugs at https://gitlab.com/jake-87/ksm/-/issues
