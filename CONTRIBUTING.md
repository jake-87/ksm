# Contrib

If you wish to contribute, please just open a PR! For bugfixes, they will be tested and merged, but for spec changes, be aware that there will most likely be a period of discussion.
